package com.example.pasifamal.roomdatabase;

import androidx.room.Dao;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface UserDAO {
     @Insert(onConflict = OnConflictStrategy.REPLACE)
    void registerUser(UserEntity userEntity);

     @Query("SELECT * FROM user where :Email and :Password")
    UserEntity login(String Email, String Password);
}
