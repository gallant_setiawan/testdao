package com.example.pasifamal;

import android.content.Intent;
import android.os.Bundle;
import android.renderscript.Sampler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pasifamal.roomdatabase.UserDAO;
import com.example.pasifamal.roomdatabase.UserDatabase;
import com.example.pasifamal.roomdatabase.UserEntity;

public class RegisterActivity extends AppCompatActivity {
    EditText edtnama , edtno, edtemail,edtpassword;
    Button btndaftar;
    TextView tvmasuk;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);
        edtnama = findViewById(R.id.edtnama_user);
        edtno = findViewById(R.id.edtno_hp);
        edtemail = findViewById(R.id.edtemail_user);
        btndaftar = findViewById(R.id.btn_daftar);
        edtpassword = findViewById(R.id.edtpassword);
        tvmasuk = findViewById(R.id.tvmasuk);



        tvmasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        btndaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtnama.getText().toString().isEmpty() ||
                edtemail.getText().toString().isEmpty() ||
                edtpassword.getText().toString().isEmpty() ||
                edtno.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(),"Data Belum Lengkap",Toast.LENGTH_SHORT).show();
                }
                else {
                    final UserEntity userEntity = new UserEntity();
                    userEntity.setEmail(edtemail.getText().toString());
                    userEntity.setNama(edtnama.getText().toString());
                    userEntity.setNohp(edtno.getText().toString());
                    userEntity.setPassword(edtpassword.getText().toString());
                    UserDatabase userDatabase = UserDatabase.getUserDatabase(getApplicationContext());
                    final UserDAO userDAO = userDatabase.userDAO();
                    Toast.makeText(getApplicationContext(),"Daftar Berhasil",Toast.LENGTH_SHORT).show();
/*
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            userDAO.registerUser(userEntity);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
Toast
                                }
                            });


                        }
                    }).start();

 */
                }
            }
        });

        }
    private Boolean validateInput(UserEntity userEntity ){
        if (userEntity.getNama().isEmpty() ||
        userEntity.getEmail().isEmpty() ||
        userEntity.getPassword().isEmpty() ||
        userEntity.getNohp().isEmpty()){
            return false;
        }
        return true;
   }

    }



