package com.example.pasifamal;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pasifamal.roomdatabase.UserDAO;
import com.example.pasifamal.roomdatabase.UserDatabase;
import com.example.pasifamal.roomdatabase.UserEntity;

public class LoginActivity extends AppCompatActivity {
    EditText etEmail, etPassword;
    TextView tvdaftar;
    Button btnmasuk;

    UserDatabase userDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        etEmail = findViewById(R.id.etemail_user);
        etPassword = findViewById(R.id.etpassword);
        tvdaftar = findViewById(R.id.tvdaftar);
        btnmasuk = findViewById(R.id.btn_masuk);

        UserDatabase.getUserDatabase(getApplicationContext());

        btnmasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            final String etemailText = etEmail.getText().toString();
            final String etpasswordText = etPassword.getText().toString();
            if (etemailText.isEmpty() &&
            etpasswordText.isEmpty()){
                Toast.makeText(getApplicationContext(),"Isi terlebih dahulu",Toast.LENGTH_SHORT).show();
            }else if(etemailText.isEmpty() ||
                    etpasswordText.isEmpty()) {
                Toast.makeText(getApplicationContext(),"Data Belum Lengkap",Toast.LENGTH_SHORT).show();
            }else {
                final UserDAO userDAO = userDatabase.userDAO();
                UserEntity userEntity = userDAO.login(etemailText, etpasswordText);
                if (userEntity == null){
                Toast.makeText(getApplicationContext(),"Akun Belum Terdaftar",Toast.LENGTH_SHORT).show();
                /*
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                        }
                    });

                 */
                } else {
                    String nama = userEntity.nama;
                    Toast.makeText(getApplicationContext(),"Data Salah",Toast.LENGTH_LONG).show();
                    startActivity(new Intent(LoginActivity.this,MainActivity.class)
                            .putExtra("nama",nama));
                }

                /*
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                    }
                }).start();

                 */
            }
            }
        });

        tvdaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

}
